package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogoutPage {
    WebDriver webDriver;

    public LogoutPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private final By logoutButton = By.xpath("//a[contains(text(),'Logout')]");

    public void clickLogout() {
        webDriver.findElement(logoutButton).click();
    }

}

package bindings;

public class DriverManagerFactory {

    public static DriverManager getDriverManager(DriverType type) {
        DriverManager driverManager;
        switch (type) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
                driverManager = new ChromeWebDriverManager();
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
                driverManager = new FirefoxWebDriverManager();
                break;
            case EDGE:
                System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/edgedriver.exe");
                driverManager = new EdgeWebDriverManager();
                break;
            case IE:
                System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
                driverManager = new IEWebDriverManager();
                break;
            default:
                System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/edgedriver.exe");
                driverManager = new SafariDriverManager();
                break;
        }

        return driverManager;
    }
}

package bindings;

import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class EdgeWebDriverManager extends DriverManager {
    @Override
    protected void createWebDriver() {
        EdgeOptions edgeOptions = new EdgeOptions();
        //additional settings
        this.webDriver = new EdgeDriver(edgeOptions);
    }
}

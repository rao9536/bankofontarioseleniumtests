package bindings;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeWebDriverManager extends DriverManager {

    @Override
    protected void createWebDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();
        // additional settings
        this.webDriver = new ChromeDriver(chromeOptions);
    }
}

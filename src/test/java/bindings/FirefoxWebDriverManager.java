package bindings;


import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FirefoxWebDriverManager extends DriverManager {

    @Override
    protected void createWebDriver() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        // additional settings
        this.webDriver = new FirefoxDriver(firefoxOptions);
    }
}

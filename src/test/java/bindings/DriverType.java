package bindings;

public enum DriverType {
    CHROME, FIREFOX, EDGE, SAFARI, IE;
}

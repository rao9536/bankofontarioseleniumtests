package tests;

import bindings.DriverManager;
import bindings.DriverManagerFactory;
import bindings.DriverType;
import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.LoginPage;
import pages.LogoutPage;
import pages.OpenAccountPage;

public class LoginRegisterLogoutTest {
    DriverManager driverManager;
    WebDriver webDriver;
    LoginPage loginPage;
    LogoutPage logoutPage;
    OpenAccountPage openAccountPage;

    @BeforeClass
    public void setup() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.FIREFOX);
        webDriver = driverManager.getWebDriver();
        webDriver.get("https://bankofontario.herokuapp.com/");
    }

    @Test(priority = 0, description = "Bank of Ontario Login Test")
    @Description("Bank of Ontario Login Test Description")
    public void loginTest() {
        SoftAssert softAssert = new SoftAssert();
        loginPage = new LoginPage(webDriver);
        loginPage.login("teststudio@gmail.com", "teststudio");
        softAssert.assertEquals(webDriver.getTitle().trim(), "Bank of Ontario");
    }

    @Test(priority = 1, description = "Register new Customer/Admin")
    public void openBankAccount() {
        SoftAssert softAssert = new SoftAssert();
        openAccountPage = new OpenAccountPage(webDriver);
        openAccountPage.clickOpenAccount();
        openAccountPage.openAccount("Vitruvi", "Calgary", 30000, 15000, 50000, 145,
                "King St West", "Toronto", "ON", "Canada", "M3B1L7", "vitruvi@gmail.com", "vitruvi");


    }

    @Test(priority = 2, description = "Bank of Ontario Logout Test")
    @Description("Bank of Ontario Logout Test Description")
    public void logoutTest() {
        SoftAssert softAssert = new SoftAssert();
        logoutPage = new LogoutPage(webDriver);
        logoutPage.clickLogout();
        softAssert.assertEquals(webDriver.getTitle().trim(), "Bank of Ontario");
        driverManager.quitWebDriver();
    }

}
